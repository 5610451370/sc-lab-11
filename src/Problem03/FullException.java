package Problem03;

public class FullException extends Exception {
	
	public FullException() {
		super();
	}
	
	public FullException(String a) {
		super(a);
	}

}
