package Problem03;

public class Main {
	
	public static void main(String[] args) {
		Refrigerator r = new Refrigerator(5);
		try {
			r.put("Chocolate");
			r.put("coffee");
			r.put("Beef");
			r.put("Chicken");
			r.put("Orange");
			System.out.print(r.toString());
			System.out.print(r.takeOut("Beef"));
			r.put("Durian");
			r.put("Icecream");
			
		}catch (FullException e) {
			System.err.print("Error: "+e.getMessage()+"\n");
		}
		finally {
			System.out.print(r.toString());
			System.out.print(r.takeOut("Cola"));
		}
	}

}
