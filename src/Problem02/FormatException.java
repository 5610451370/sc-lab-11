package Problem02;

public class FormatException extends Exception {
	
	public FormatException() {
		super();
	}
	
	public FormatException(String a) {
		super(a);
	}

}
