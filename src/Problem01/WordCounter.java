package Problem01;

import java.util.HashMap;

public class WordCounter {
	private String message;
	private HashMap<String,Integer> wordCount;
	
	public WordCounter(String message){
		this.message = message;
	}
	
	public void Count(){
		String spritt[] = message.split(" ");
		wordCount = new HashMap<String, Integer>();
		for (int i = 0; i < spritt.length; i++) {
            if (!wordCount.containsKey(spritt[i])) {
            	wordCount.put(spritt[i], 1);
            } 
            else {
            	wordCount.put(spritt[i], (Integer) wordCount.get(spritt[i]) + 1);
            }
        }
	}
	
	public int hasWord(String word){
		int sum = 0;
		for(int i = 0; i < wordCount.size();i++){
			if(wordCount.containsKey(word)){
				sum = wordCount.get(word);
			}
		}
		
		return sum;
	}
	
	
	
	
}
